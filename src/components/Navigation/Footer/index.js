import React, { useState } from 'react';

import './Footer.scss';

import Modal from '../../Modal';
import Imprint from '../../Information/Imprint';
import Privacy from '../../Information/Privacy';

const Footer = () => {
	const [isOpen, setIsOpen] = useState(false);
	const [modalContent, setModlaContent] = useState({
		title: null,
		content: null
	});

	const openModal = (title, content) => {
		setIsOpen(true);
		setModlaContent({
			title,
			content
		});
	};

	const closeModal = () => {
		setIsOpen(false);
		setModlaContent({
			title: null,
			content: null
		});
	};

	const openImprint = () => {
		openModal('Imprint', <Imprint />);
	};

	const openPrivacy = () => {
		openModal('Privacy', <Privacy />);
	};

	return (
		<div className="footer">
			<div className="container">
				<div className="row">
					<div className="mx-auto">
						<h4 className="title">Jobsi</h4>
						<hr className="m-0 my-2" />
						<ul className="list-unstyled">
							<li>
								<a href="#" onClick={openImprint}>
									Imprint
								</a>
							</li>
							<li>
								<a href="#" onClick={openPrivacy}>
									Privacy
								</a>
							</li>
						</ul>
					</div>
					
				</div>
			</div>
			<Modal isOpen={isOpen} handleClose={closeModal} title={modalContent.title} large={false}>
				{modalContent.content}
			</Modal>
		</div>
	);
};

export default Footer;
