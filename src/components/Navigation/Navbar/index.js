import React from 'react';

import './Navbar.scss';

const Navbar = () => (
	<nav className="navbar navbar-dark">
		<div className="container">
			<h4 className="mx-auto"><a className="mx-auto mr-auto" href="/">
				Jobsi
			</a></h4>
		</div>
	</nav>
);

export default Navbar;
