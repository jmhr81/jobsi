import React, { useState } from 'react';

import './List.scss';
import Job from '../Jobs';
import Modal from '../Modal';
import Details from '../Details';

const List = ({ jobs }) => {
	const [isOpen, setIsOpen] = useState(false);
	const [modalContent, setModalContent] = useState({
		title: null,
		content: null
	});

	const openModal = (job) => {
		setIsOpen(true);
		setModalContent({
			title: job.Title,
			content: <Details jobs={job} />
		});
	};

	const closeModal = () => {
		setIsOpen(false);
		setModalContent({
			title: null,
			content: null
		});
	};

	const rows = [];
	const jobsPerRow = 3;

	for (let i = 0; i < Math.ceil(jobs.length / jobsPerRow); i++) {
		const tmp = [];
		for (let k = i * jobsPerRow; k < Math.min((i + 1) * jobsPerRow, jobs.length); k++) {
			
			tmp.push(<Job key={k} job={jobs[k]} openModal={openModal} />);
		}
		rows.push(tmp);
	}

	return (
		<div className="List">
			<p className="count">Jobs: {jobs.length}</p>
			{rows.map((row, i) => (
				<div className="row" key={i}>
					{row.map((jobs, j) => (
						<div className="col-12 col-md-4 d-flex align-items-stretch" key={j}>
							{jobs}
						</div>
					))}
				</div>
			))}
			<Modal isOpen={isOpen} handleClose={closeModal} title={modalContent.title}>
				{modalContent.content}
			</Modal>
		</div>
	);
};

export default List;
