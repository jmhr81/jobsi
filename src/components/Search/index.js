import React from 'react';

import './Search.scss';

const Search = (props) => {
	const { updateParentTerm, updateParentOrder } = props;
	let timeout;

	const handleBlur = async (e) => {
		const term = e.target.value.trim();
		updateParentTerm(term);
	};

	const handleChange = async (e) => {
		const term = e.target.value.trim();

		clearTimeout(timeout);

		timeout = setTimeout(() => {
			updateParentTerm(encodeURIComponent(term));
		}, 750);
	};

	const handleSearch = async (e) => {
		if(e.key == 'Enter'){
			const term = e.target.value.trim();
			updateParentTerm(encodeURIComponent(term));
		}
		
	};
	return (
		<div className="Search">
			<div className="row my-5">
				<div className="col-12 col-md-6 offset-md-2">
					<div className="input-group">
						<input
							type="text"
							className="form-control"
							placeholder="Search for jobs like 'Java'"
							aria-label="Search jobs"
							aria-describedby="search"
							onBlur={handleBlur}
							onChange={handleChange}
							onKeyPress={e => handleSearch(e)}
							id="search"
							autoComplete="off"
							autoFocus
						/>
						<div className="input-group-append">
							<span className="input-group-text" id="search">
								<i className="fas fa-search"></i>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Search;
