import React from 'react';

import Spinner from './Spinner';

const Loader = () => (
	<div className="Loader">
		<div className="d-md-none">
			<Spinner />
		</div>
		
	</div>
);

export default Loader;
