import React, { useState } from 'react';

import './Jobs.scss';
import { GoogleJobs } from '../../handler/Google';


const Jobs = ({ job, openModal }) => {
	const { title, company, created_at, url, description, location } = job;
	const { urls } = url;
	

	const [isLoading, setIsLoading] = useState(false);
	const [text, setText] = useState('Job Description');

	const service = new GoogleJobs();

	const handleDetails = async () => {
		setIsLoading(true);
		const res = await service.getCompanyDetails(company);
		setIsLoading(false);


		if (res.data.itemListElement.length === 0 || !res.data.itemListElement[0].result.hasOwnProperty('detailedDescription')) {
			setText('Job Description');
			openModal({"googleData": '', "JobDescription": description, "Title": title, "Company": company, 'isGoogleResult': false});
		} else {
			setText('Job Description');
			openModal({"googleData": res.data.itemListElement[0].result.detailedDescription, "JobDescription": description, "Title": title, "Company": company, 'isGoogleResult': true});
		}
	};

	return (
		<div className="Jobs card" style={{ flex: 1 }}>
			<div className="card-body">
				<h5 className="card-title">{title}</h5>
				<h6 className="card-subtitle mb-2">Opened: {created_at}</h6>
				<p className="card-text">Company: {company}</p>
                <p className="card-text">Location: {location}</p>
			</div>
			<div className="card-footer d-flex align-items-center">
				{isLoading ? (
					<button className="btn btn-primary btn-block" type="button" disabled>
						<span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
					</button>
				) : (
					<button type="button" className="btn btn-primary btn-block" onClick={handleDetails}>
						{text}
					</button>
				)}
				<a href={url} className="btn btn-secondary btn-block ml-1 mt-0" target="_blank" rel="noopener noreferrer">
					View Jobposting
				</a>
			</div>
		</div>
	);
};

export default Jobs;
