import React from 'react';


import './Details.scss';

const Details = ({ jobs }) => {
	const { Company, JobDescription, googleData, isGoogleResult } = jobs;
	


	var div = document.createElement("div");
	div.innerHTML = JobDescription;
	
	if(!isGoogleResult){

		return (
			<div className="Details">
				<div className="section">
					<h4>Company</h4>
					<p>{Company}</p>
				</div>
				<div className="section">
					<h4>Description</h4>
					<p>{div.innerText}</p>
				</div>
				<div className="section">
					<h4>Company Details</h4>
					<p>-</p>
				</div>
			</div>
		);
	}else{
		return (
			<div className="Details">
				<div className="section">
					<h4>Company</h4>
					<p>{Company}</p>
				</div>
				<div className="section">
					<h4>Description</h4>
					<p>{div.innerText}</p>
				</div>
				<div className="section">
					<h4>Company Details</h4>
					<p>{googleData.hasOwnProperty('articleBody')  ? googleData.articleBody : ''}</p>
					<a href={googleData.url} className="btn btn-secondary btn-block ml-1 mt-0" target="_blank" rel="noopener noreferrer">{Company}</a>
				</div>
			</div>
		);
	}

	
};

export default Details;
