import React, { Component } from 'react';

import './Result.scss';

import { GithubJobs } from '../../handler/GithubJobs';

import Loader from '../Loader';

import List from '../List';

import Alert from '../Alert';

class Result extends Component {
	constructor(props) {
		super(props);

		this.service = new GithubJobs();

		this.state = {
			data: null,
			isLoading: false,
			error: ''
		};
	}

	componentDidMount = async () => {
		const { searchTerm } = this.props;
		if (searchTerm.length) await this.fetchJobs();
	};

	componentDidUpdate = async (prevProps) => {
		if (prevProps.searchTerm !== this.props.searchTerm)
			await this.fetchJobs(this.props.searchTerm);
	};

	

	fetchJobs = async (term) => {
		if (!term.length) this.setState({ data: null });
		else {
			this.setState({ isLoading: true, data: null, error: '' }, async () => {
				try {
					const res = await this.service.searchJobs(term);
					this.setState({ data: res.data, isLoading: false });
				} catch (err) {
					this.setState({
						isLoading: false,
						error: 'Failed to fetch jobs.'
					});
				}
			});
		}
	};

	render() {
		const { data, isLoading, error } = this.state;
		
		if (isLoading) return <Loader />;
		if (error.length)
			return (
				<div className="row">
					<div className="col-12 offset-0 col-md-6 offset-md-3">
						<Alert title="Error!" msg={error} type="danger" />
					</div>
				</div>
			);

		if (data === null) return null;

		if (!data.length) return <div className="hint">No results</div>;

		return (
			<div className="Result">
				<List jobs={data} />
			</div>
		);
	}
}

export default Result;
