# jobsi (Mashup-App WBS 2020)

- Jonas Mohr
- Applikation: [https://jobsi-jm.herokuapp.com/](https://jobsi-jm.herokuapp.com/)

----

## Einleitung

jobsi ist eine Mashup-App, welche zwei Rest Apis miteinander verbindet:
Dabei wird github-jobs verwendet, diese API stellt offene Stellen zur Verfügung. Desweiteren wird "Google Knowledge Graph Search API" verwendet, diese ermöglicht es weitere Informationen zu den Firmen, welche nach neuen Mitarbeitern suchen, zu finden.
Ziel ist den Umweg über eine Suchmaschine zu umgehen, um zu offenen Stellen Informationen über die ggf. unbekannten Firmen bereitzustellen.


## API´s

### github-jobs-api
[https://jobs.github.com/api](https://jobs.github.com/api)
Bietet die Möglichkeit Jobs, welche auf jobs.github.com gelistet sind abzufragen.

### Google Knowledge Graph Search API
[https://developers.google.com/knowledge-graph](https://developers.google.com/knowledge-graph) 
Google Knowledge Graph bietet die möglichkeit Suchabfragen an google zu stellen und treffende Ergebnisse von google zurückgeliefert zubekommen. Google fragt dazu weitere Dienste wie bspw. Wikipedia ab und fasst diese Ergebnisse zusammen.

## Technologien

- React (Frontend JS Framework)
- Bootstrap (CSS Framework)

## Farben
![preview](./Screenshots/color-palette.PNG)

## Installation

### Standard

Um die App zu installieren wird Node.js und der Paketmanager npm benötigt.

1. Clone das GIT-Repository [https://git.thm.de/jmhr81/jobsi.git](https://git.thm.de/jmhr81/jobsi.git)
2. Öffne ein Terminal im Ordner des geclonten Projekts
3. Führe `npm install` im Terminal aus, um die Dependencies zu installieren
5. Führe `npm start` im Terminal aus, um die Anwendung zu starten
6. Die Anwendung startet nun und sollte unter `http://localhost:3000` verfügbar sein

